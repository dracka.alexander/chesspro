#!/usr/bin/env python3

import sys
import argparse
import requests
import os
import re
import logging
import json


class ChessProject:
    """
    Base class for manipulation with chess.com endpoint.
    Based on this docs https://www.chess.com/news/view/published-data-api#pubapi-endpoint-games-pgn
    """
    def __init__(self):
        print("""\
        ,....,
      ,::::::<
     ,::/^\\"``.
    ,::/, `   e`.
   ,::; |        '.
   ,::|  \\___,-.  c)
   ;::|     \\   '-'
   ;::|      \\
   ;::|   _.=`\\
   `;:|.=` _.=`\\
     '|_.=`   __\\
     `\\_..==`` /
      .'.___.-'.
     /          \\
jgs ('--......--')
    /'--......--'\\
    `"--......--"
        """)
        examples = f''' 
 {sys.argv[0]} -p alexander30       # additional details about a player
 {sys.argv[0]} -p alexander30 -g    # Array of Daily Chess games that a player is currently playing.
 {sys.argv[0]} -p alexander30 -tm   # Array of Daily Chess games where it is the player's turn to act. 
 {sys.argv[0]} -p alexander30 -a    # Array of monthly archives available for this player.
 {sys.argv[0]} -ps alexander30      # Detail statistics of player
 {sys.argv[0]} -titled GM                # List of GM titled-player usernames
 {sys.argv[0]} -p alexander30 -ca 2012/4-2021/2    # download all games from 2012/1 to 2021/2
 {sys.argv[0]} -p alexander30 -ca 2012/4-2021/2 -R # download only rapid games from 2012/1 to 2021/2
 {sys.argv[0]} -p alexander30 -ca 2012/4-2021/2 -B # download only rapid games from 2012/1 to 2021/2
 {sys.argv[0]} -p alexander30 -ca 2012/4-2021/2 -R # download only rapid games from 2012/1 to 2021/2
 
 Known issues:
 - Importing 960 games can failed as chess.com is suing unrecognizable tag [SetUp "1"]
 - Ideal way to save analysed game in scid is to imported pgn file into Database
  
'''
        # TODO issue with -tm option and Alexander30 vs alexander30
        # todo are all options works?
        # todo check return type & hints and not explicit variables!

        self.pgn_file = 'exported_games.pgn'
        self.pgn_path = os.path.join(os.path.dirname(sys.argv[0]), self.pgn_file)
        self.url = 'https://api.chess.com/pub/'
        # self.logger = logging.Logger()
        self.args = None
        logging.basicConfig(level=logging.DEBUG, filename='ExportChessCom.log')
        self._init_parser(examples)

    def _init_parser(self, examples):
        parser = argparse.ArgumentParser(description='testing tool for exporting chess games in pgn from chess.com',
                                         formatter_class=argparse.RawTextHelpFormatter, epilog=f'''
EXAMPLES:
{examples}''')
        parser.add_argument('-tm', '--to_move', help="Get array of daily chess games where it is the player's turn to act", action='store_true')
        parser.add_argument('-g', '--games', help='Array of Daily Chess games that a player is currently playing.', action='store_true')
        parser.add_argument('-a', '--archives', help='Get array of monthly archives available for this player', action='store_true')
        parser.add_argument('-ca', '--complete_archives', help='Period of time in format: YYYY/MM-YYYY/MM', action='store')
        parser.add_argument('-c', '--clubs', help='List of clubs for player', action='store_true')
        parser.add_argument('-m', '--matches', help='List of Team matches', action='store_true')
        parser.add_argument('-t', '--tournaments', help='List of tournaments', action='store_true')
        parser.add_argument('-p', '--player', help='Get additional details about a player in a game', action='store')
        parser.add_argument('-ps', '--player_stats', help="Get ratings, win/loss, and other stats about a player's game play, tactics, lessons and Puzzle Rush score.", action='store')
        parser.add_argument('-titled', '--titled_players', help='List of titled-player usernames for each title [GM, WGM, IM, WIM, FM, WFM, NM, WNM, CM, WCM] ', action='store')

        parser.add_argument('-D', '--Daily', help='Filter only daily games (> 1 day)', action='store_true')
        parser.add_argument('-R', '--Rapid', help='Filter only rapid games (10 min - 30 min)', action='store_true')
        parser.add_argument('-Blitz', '--Blitz', help='Filter only blitz games (3 min - 10 min)', action='store_true')
        parser.add_argument('-Bullet', '--Bullet', help='Filter only bullet games (1 min - 2 min)', action='store_true')
        parser.add_argument('-database', '--database_of_games', help='Download all games for each Title.', action='store')
        self.parser = parser

    # ******************************************************************************************************************
    # Main code
    # ******************************************************************************************************************

    def archives(self) -> dict:
        """Return an array of monthly archives available for this player."""
        result = requests.get(os.path.join(self.url, 'player', self.args.player, 'games/archives'))
        if result.status_code != 200:
            raise Exception
        result_json = json.loads(result.text)
        return result_json

    def complete_archives(self) -> None:
        """Array of Live and Daily Chess games that a player has finished in certain period of time."""
        self.calculate_months_and_execute()
        print('Export of games finished.')

    def clubs(self):
        """List of clubs the player is a member of, with joined date and last activity date."""
        result = requests.get(os.path.join(self.url, 'player', self.args.player, 'clubs'))
        if result.status_code != 200:
            raise Exception
        result_json = json.loads(result.text)
        return result_json

    def database_of_games(self):
        self.download_all_games_for_all_titled_players()

    def matches(self):
        """List of Team matches the player has attended, is participating or is currently registered."""
        result = requests.get(os.path.join(self.url, 'player', self.args.player, 'matches'))
        if result.status_code != 200:
            raise Exception
        result_json = json.loads(result.text)
        return result_json

    def tournaments(self):
        """List of tournaments the player is registered, is attending or has attended in the past."""
        result = requests.get(os.path.join(self.url, 'player', self.args.player, 'tournaments'))
        if result.status_code != 200:
            raise Exception
        result_json = json.loads(result.text)
        return result_json

    def is_player_online(self) -> bool:
        None
        None
        # todo

    def player(self):
        """Print information about player profile"""
        result = requests.get(os.path.join(self.url, 'player', sys.argv[2]))
        result_json = json.loads(result.text)
        return result_json

    def titled_players(self) -> list:
        """ List of titled-player usernames. Expecting valid title: GM, WGM, IM, WIM, FM, WFM, NM, WNM, CM, WCM."""
        result = requests.get(os.path.join(self.url, 'titled', sys.argv[2]))
        if result.status_code != 200:
            raise Exception
        result_json = json.loads(result.text)
        return result_json['players']

    def player_stats(self):
        result = requests.get(os.path.join(self.url, 'player', sys.argv[2], 'stats'))
        if result.status_code != 200:
            raise Exception
        result_json = json.loads(result.text)
        return result_json

    def games(self):
        result = requests.get(os.path.join(self.url, 'player', sys.argv[2], 'games'))
        if result.status_code != 200:
            raise Exception
        result_json = json.loads(result.text)
        return result_json

    def to_move(self):
        url = os.path.join(self.url, 'player', sys.argv[2], 'games/to-move')
        result = requests.get(url)
        if result.status_code != 200:
            raise Exception
        result_json = json.loads(result.text)
        return result_json

    # ******************************************************************************************************************
    # Helping methods
    # ******************************************************************************************************************

    def download_all_games_for_all_titled_players(self):
        # Download list of GM/IM/FM players
        # Download all games for each player
        list = self.titled_players()
        for player in list:
            self.args.player = player
            for year in range(2008, 2022):
                for month in range(1, 13):
                    self.pgn_path = os.path.join(os.getcwd(), f'{player}.pgn')
                    self.download_games_for_year_and_month(year, month)
        pass

    def execute_arg(self) -> ():
        """
        TODO add logging
        :return:
        """
        self.args = self.parser.parse_args()
        return (getattr(self, arg)() for arg, value in self.args.__dict__.items() if value)

    def calculate_months_and_execute(self) -> None:
        """Parse format into YYYY/MM-YYYY/MM and execute export of games for every month."""
        date_list = re.split('/|-', self.args.complete_archives)
        start_year = int(date_list[0])
        start_month = int(date_list[1])
        end_year = int(date_list[2])
        end_month = int(date_list[3])

        for year in range(start_year, end_year + 1):
            if year == start_year:
                for month in range(start_month, 13 if year != end_year else end_month + 1):
                    self.download_games_for_year_and_month(year, month)
            else:
                for month in range(1, 13 if year != end_year else end_month + 1):
                    self.download_games_for_year_and_month(year, month)

    def download_games_for_year_and_month(self, year: int, month: int) -> None:
        """
        Core Logic of the actual downloading data
        Download games for specific year and month, with optional filter (daily, rapid, blitz, bullet games)
        @param year: Year
        @param month: Month of the year
        """
        if self.args.player is None:
            raise Exception(f'Unknown user:{self.args.player}, aborting.')

        logging.info(f'{self.args.player}Downloading games for date: {year}/{month}')
        result = requests.get(os.path.join(self.url, 'player', self.args.player, 'games', year, month, 'pgn'))
        logging.debug(f'Status code:{result.status_code}')
        # todo filter pgn
        # todo remove game with position (thematic games)
        filtered_games = []
        for game in result.text.split('[Event'):
            game = '[Event' + game
            index = game.find('%clk')
            if index == -1 or '960' in game:
                # %clk was not found -> ignore this game TODO handling of games win without any move played 'Termination'
                continue
            time = game[index+5:index+13]
            hours = int(time.split(':')[0])
            minutes = int(time.split(':')[1])
            if self.args.Daily and hours != '':
                if hours >= 1:
                    filtered_games.append(game)
            elif self.args.Rapid:
                if 10 <= minutes < 60:
                    filtered_games.append(game)
            elif self.args.Blitz:
                if 3 <= minutes <= 5:
                    filtered_games.append(game)
            elif self.args.Bullet:
                if minutes <= 2:
                    filtered_games.append(game)
            else:
                # no filter applied
                filtered_games.append(game)

        # if filtered-games are empty write it in print/log
        if len(filtered_games) == 0:
            logging.debug(f'No games for {month}/{year}')
            print(f'No games for {month}/{year}')
        else:
            self.store_list_of_pgn_in_file(filtered_games)
            logging.debug(f'Date {year}/{month} were stored in file.')
            print(f'Date {year}/{month} were stored in file.')

    def store_list_of_pgn_in_file(self, pgn_list) -> None:
        """Store list of pgn in file."""
        for pgn in pgn_list:
            self.store_pgn_in_file(pgn)

    def store_pgn_in_file(self, pgn) -> None:
        """Store pgn in file"""
        logging.debug('Adding to file...')
        with open(self.pgn_path, 'a') as file:
            file.write(pgn)
            file.write('\n\n')      # Prevent corrupting pgn format for SCID import

    def export_games_from_endpoint(self):
        # todo remove obsolete probably
        self.calculate_months_and_execute()
        pass

    def print_player_info(self):
        """Print information about player profile"""
        result = requests.get(os.path.join(self.url, 'player', self.parser.user))

        return result


def main():
    """Script execution entry point: communication with chess.com endpoint."""
    print('ChessProject greetings you! 1.Nf3, your turn...\nUse -h for help.')
    for result in ChessProject().execute_arg():
        if result:
            return result


if __name__ == '__main__':
    sys.exit(main())
